package utils

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

// Init initialises basic data structures required.
func Init() {
	HostnameToCount = make(map[string]int)

	dir, _ := os.Getwd()
	err := godotenv.Load(dir + "/files/config/main.env")
	if err != nil {
		log.Println("[Init] Unable to load .env fie. ", err)
	}

	log.Println("[Init] Environment loaded successfully.")

	data, err := readSampleData(dir + "/files/data/sampleData.csv")
	if err != nil {
		fmt.Println("[Init] Unable to read SampleData: ", err)
	}

	getActiveIPsPerHostname(data)
}
