package utils

import (
	"os"
	"reflect"
	"testing"
)

var testData = [][]string{
	[]string{"IP", "Hostname", "Active"},
	[]string{"127.0.0.1", "mta-prod-1", "true"},
	[]string{"127.0.0.2", "mta-prod-1", "false"},
	[]string{"127.0.0.3", "mta-prod-2", "true"},
	[]string{"127.0.0.4", "mta-prod-2", "true"},
	[]string{"127.0.0.5", "mta-prod-2", "false"},
	[]string{"127.0.0.6", "mta-prod-3", "false"},
}

func Test_getActiveIPsPerHostname(t *testing.T) {

	HostnameToCount = make(map[string]int)

	type args struct {
		data [][]string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Default",
			args: args{
				data: testData,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			getActiveIPsPerHostname(tt.args.data)
			if _, ok := HostnameToCount["mta-prod-1"]; !ok {
				t.Error("Expected data to be present but it's not.")
			}
		})
	}
}

func Test_readSampleData(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want [][]string
	}{
		{
			name: "Test-1",
			args: args{
				path: os.Getenv("GOPATH") + "/src/gitlab.com/yogeshbdesai1/mta-hosting-optimizer/files/data/sampleData.csv",
			},
			want: testData,
		},
		{
			name: "Test-2",
			args: args{
				path: "",
			},
			want: [][]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := readSampleData(tt.args.path); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("readSampleData() = %v, want %v", got, tt.want)
			}
		})
	}
}
