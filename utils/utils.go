package utils

import (
	"encoding/csv"
	"log"
	"os"
)

// readSampleData opens and reads the sample data returns it as slice
func readSampleData(path string) ([][]string, error) {

	file, err := os.Open(path)
	if err != nil {
		log.Println("[readSampleData] Unable to open sampleData.csv file.")
		return [][]string{}, err
	}

	log.Println("[readSampleData] Data file opened successfully.")
	defer file.Close()

	data, err := csv.NewReader(file).ReadAll()
	if err != nil {
		log.Println("[readSampleData] ", err)
		return [][]string{}, err
	}
	return data, nil

}

// getActiveIPsPerHostname prepares map of Hostname to active count
func getActiveIPsPerHostname(data [][]string) {
	for i, line := range data {
		if i == 0 {
			continue
		}

		host := line[1]
		active := line[2]
		v, ok := HostnameToCount[host]
		if ok { // updating existing Hostname entry
			if active == "true" {
				HostnameToCount[host] = v + 1
			}
		} else { // adding new hostname entry to Map

			if active == "true" {
				HostnameToCount[host] = 1
			} else {
				HostnameToCount[host] = 0
			}
		}
	}

	log.Println("[getActiveIPsPerHostname] Data Map is ready.")
}
