package api

import (
	"log"
	"net/http"
)

func Init() {

	http.Handle("/gethostnames", http.HandlerFunc(GetHostNames))

	log.Println("[Init] Api routes initialized.")

}
