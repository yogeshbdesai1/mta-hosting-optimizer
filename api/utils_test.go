package api

import (
	"os"
	"testing"
)

func Test_getThreshold(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			name: "simple-1",
			want: 1,
		},
		{
			name: "simple-2",
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.name == "simple-2" {
				os.Setenv("threshold", "2")
			}
			if got := getThreshold(); got != tt.want {
				t.Errorf("getThreshold() = %v, want %v", got, tt.want)
			}
		})
	}

	os.Unsetenv("threshold")
}
