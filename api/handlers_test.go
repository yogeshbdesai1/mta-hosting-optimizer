package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yogeshbdesai1/mta-hosting-optimizer/utils"
)

func TestGetHostNames(t *testing.T) {

	Init()
	utils.HostnameToCount = make(map[string]int)

	utils.HostnameToCount["mta-prod-1"] = 1
	utils.HostnameToCount["mta-prod-2"] = 2
	utils.HostnameToCount["mta-prod-3"] = 0

	req := httptest.NewRequest(http.MethodGet, "/gethostnames", nil)
	w := httptest.NewRecorder()

	handler := http.HandlerFunc(GetHostNames)
	handler.ServeHTTP(w, req)

	if status := w.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	resp1 := fmt.Sprintf("%s\n%s", "mta-prod-1", "mta-prod-3")
	resp2 := fmt.Sprintf("%s\n%s", "mta-prod-3", "mta-prod-1")

	if w.Body.String() != resp1 && w.Body.String() != resp2 {
		t.Errorf("handler returned unexpected body: got %v want %v or %v",
			w.Body.String(), resp1, resp2)
	}
}
