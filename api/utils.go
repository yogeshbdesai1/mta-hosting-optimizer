package api

import (
	"log"
	"os"
	"strconv"
)

// getThreshold returns the threshold already set.
func getThreshold() int {
	env, ok := os.LookupEnv("threshold")
	if !ok {
		log.Println("[getThreshold] Environment variable not set, using defaults")
		return 1 // set default as 1
	}

	threshold, err := strconv.Atoi(env)
	if err != nil {
		log.Println("[getThreshold] Error in string conversion. ", err)
	}

	return threshold
}
