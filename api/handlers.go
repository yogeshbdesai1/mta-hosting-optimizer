package api

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/yogeshbdesai1/mta-hosting-optimizer/utils"
)

// GetHostNames handler for getting Hostnames which are not efficiently active
func GetHostNames(w http.ResponseWriter, r *http.Request) {
	var threshold int = getThreshold()
	var result string

	for k, v := range utils.HostnameToCount {
		if v <= threshold {
			result = strings.TrimSpace(fmt.Sprintf("%v\n%v", result, k))
		}
	}

	w.Write([]byte(result))
}
