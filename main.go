package main

import (
	"log"
	"net/http"

	"gitlab.com/yogeshbdesai1/mta-hosting-optimizer/api"
	"gitlab.com/yogeshbdesai1/mta-hosting-optimizer/utils"
)

func init() {

	api.Init()
	utils.Init()
}

func main() {

	log.Println("[main] Started listening at :9000 ")
	http.ListenAndServe(":9000", nil)

}
