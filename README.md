[![pipeline status](https://gitlab.com/yogeshbdesai1/mta-hosting-optimizer/badges/main/pipeline.svg)](https://gitlab.com/yogeshbdesai1/mta-hosting-optimizer/-/commits/main)

Hi everyone!
This is the case study. It is very basic web appliation for Mail Transfer Agent Hosting Optimizer.

### Go Test coverage
go test -cover ./...
- ok      gitlab.com/yogeshbdesai1/mta-hosting-optimizer          coverage: 50.0% of statements
- ok      gitlab.com/yogeshbdesai1/mta-hosting-optimizer/api      coverage: 93.8% of statements
- ok      gitlab.com/yogeshbdesai1/mta-hosting-optimizer/utils    coverage: 64.7% of statements


### Docker:
- Added Dockerfile for containerization. Did optimization to reduce the image size. Previous size was >300 MBs. Now it's just 1.6MBs.
- Also added the image file at Gitlab registry. You can check it: https://gitlab.com/yogeshbdesai1/mta-hosting-optimizer/container_registry/2363216
- Fyi, you can set environment variable in the docker compose file and run the application. No need to re-build the docker image.
- This is not deployed to any environment yet.
